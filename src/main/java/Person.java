import java.time.LocalDate;
import java.util.Objects;

public class Person {
    public enum Sex {
        MALE, FEMALE
    }
    private String name;
    private LocalDate dateOfBirth;
    private Sex gender;
    private String emailAddress;
    private int iq;

    public Person(String name, LocalDate dateOfBirth, Sex gender, String emailAddress, int iq) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.emailAddress = emailAddress;
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name) && dateOfBirth.equals(person.dateOfBirth) && gender == person.gender && emailAddress.equals(person.emailAddress);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", gender=" + gender +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dateOfBirth, gender, emailAddress);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Sex getGender() {
        return gender;
    }

    public void setGender(Sex gender) {
        this.gender = gender;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
