import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        //ex1
        Person p1 = new Person("Danny Peterson", LocalDate.of(1985,5,10), Person.Sex.MALE,"danny@gmail.com",110);
        Person p2 = new Person("Kelly Donalds", LocalDate.of(1999,4,10), Person.Sex.FEMALE,"kellyd@gmail.com",87);
        Person p3 = new Person("Mara Lara", LocalDate.of(1995,1,25), Person.Sex.FEMALE,"mlara@yahoo.com",85);
        Person p4 = new Person("Rebecca B", LocalDate.of(1958,8,8), Person.Sex.FEMALE,"rebecca_b@gmail.com",97);
        Person p5 = new Person("Peter Pan", LocalDate.of(1996,7,18), Person.Sex.MALE,"peter@pan.com",120);

        List<Person> persons = new ArrayList<>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);
        persons.add(p5);

        //ex2
        Set<Person> ex2Set = getStartsMContainsA(persons);

        //ex3
        System.out.println("Youngest person:");
        System.out.println(getYoungest(persons));

        //ex4
        Set <String> strings = randomString();
        System.out.println("max string: " + strings.stream().max(String::compareTo).get());;
        //ex5
        randomInts();

        //ex6
        Map<Person, Integer> grades = new HashMap<>();
        grades.put(p1,10);
        grades.put(p5,44);
        grades.put(p2,89);
        grades.put(p3,5);
        grades.put(p4,100);
        System.out.println("Number of people with grades over 10: " + grades.entrySet().stream().filter(e -> e.getValue()>10).count());
        //ex7
        strings.stream().sorted(Comparator.reverseOrder());
        //ex8
        persons.stream().sorted(Comparator.comparing(Person::getName));

        //ex9
        persons.stream().filter(p -> p.getIq() < 100).forEach(System.out::println);
        //ex10
        Person p = null;
        Optional<Person> o1 = Optional.of(p1);
        Optional<Person> o2 = Optional.ofNullable(p);
        o1.ifPresent(o -> o.setGender(Person.Sex.FEMALE));
        System.out.println(o2.isPresent()); //false

        //ex11
        System.out.println("Shortest string:");
        System.out.println(strings.stream().min(Comparator.comparingInt(String::length)).get());






    }


    private static Set<Person> getStartsMContainsA(List<Person> personList){

        Set set =  personList.stream().filter(person -> person.getName().startsWith("M") && person.getName().contains("a")).collect(Collectors.toSet());
        System.out.println("elements containing the letter \"a\" that start with \"M\":");
        set.stream().forEach(System.out::println);
        return set;
    }

    private static Person getYoungest(List<Person> personList){
        Comparator<Person> ageComparator = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o2.getDateOfBirth().compareTo(o1.getDateOfBirth());
            }
        };
        return personList.stream().min(ageComparator).get();
    }

    /**
     * The String equals() method compares the original content of the string. It compares values of string for equality. String class provides two methods:
     * public boolean equals(Object another) compares this string to the specified object.
     * public boolean equalsIgnoreCase(String another) compares this String to another string, ignoring case.
     */

    private static Set<String> randomString(){
        Random r = new Random();
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        Set<String> strings =  Stream.generate(() -> random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(random.nextInt(100)+1)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString()).limit(5).collect(Collectors.toSet());
      //  strings.forEach(System.out::println);
        return strings;

    }

    private static void randomInts(){
        Random random = new Random();


        System.out.println("Number of ints generated: " + random.ints(random.nextInt(Integer.MAX_VALUE)+1)
                .count());

        //dont know what exponential to the number means :D


    }





}
